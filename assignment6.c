#include <stdio.h>

int main()
{
    int prime[10],i;
    prime[0]=2;
    prime[1]=3;
    prime[2]=5;
    prime[3]=11;
    prime[4]=13;
    prime[5]=17;
    prime[6]=19;
    prime[7]=23;
    printf("before insertion:\n");
    for(i=0;i<8;i++)
    {
        printf(" %d ",prime[i]);
    }
    for(i=8;i>3;i--)
    {
        prime[i]=prime[i-1];
    }
    prime[3]=7;
    printf("\nafter insertion:\n");
    for(i=0;i<9;i++)
    {
        printf(" %d ",prime[i]);
    }
    return 0;
}