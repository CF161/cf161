#include <stdio.h>


int main()
{
    int c,r,i,j;
    printf("enter number of rows in matrix");
    scanf("%d",&r);
    printf("enter number of columns in matrix");
    scanf("%d",&c);
    int a[r][c];
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("enter element in row %d and column %d",i+1,j+1);
            scanf("%d",&a[i][j]);
        }
    }
    printf("entered matrix is :\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("\t%d",a[i][j]);
        }
        printf("\n");
    }
    printf("transpose of the entered matrix is :\n");
    for(i=0;i<c;i++)
            {
                for(j=0;j<r;j++)
                {
                    printf("\t%d",a[j][i]);
                }
                printf("\n");
            }
    return 0;
}
