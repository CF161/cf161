#include<stdio.h>
#include<math.h>
int main()
{
   float a, b, c, d;
   printf("enter coeffecients a,b,c of quadratic equation ax^2+bx+c");
   scanf("%f%f%f",&a,&b,&c);
   d=(b*b)-(4*a*c);
   if (a==0)
   printf("equation is not quadratic");
   else if (d==0)
   printf("roots are real and equal- r1=r2=%f",(-b)/(2*a));
   else if (d>0)
   printf("roots are real and distinct- r1=%f, r2=%f",(-b+sqrt(d))/(2*a),(-b-sqrt(d))/(2*a));
   else
   printf("roots are imaginary and distinct-r1=%f + i%f, r2=%f - i%f",(-b)/(2*a),(sqrt(-d))/(2*a),(-b)/(2*a),(sqrt(-d))/(2*a));
   return 0;
}
   
