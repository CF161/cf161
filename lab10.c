#include <stdio.h>


int main()
{
    FILE *a;
    char c;
    a=fopen("input.txt","w");
    printf("enter\n");
    while ((c=getchar())!=EOF)
    {
        putc(c,a);
    }
    fclose(a);
    a=fopen("input.txt","r");
    printf("file contents:\n");
     while ((c=getc(a))!=EOF)
    {
        printf("%c",c);
    }
    fclose(a);
    return 0;
}
