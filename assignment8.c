#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i;
    char s[100];
    printf("enter string in uppercase\n");
    gets(s);
    for(i=0;s[i]!='\0';i++)
    {
        if(s[i]>='A'&&s[i]<='Z')
        {
            s[i]=s[i]+32;
        }
    }
    printf("entered string in lower case:\n");
    puts(s);
    return 0;
}
